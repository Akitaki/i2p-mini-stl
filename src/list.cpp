#include "../header/I2P2_List.h"
#include "../header/logger.h"

#include <iostream>

namespace I2P2 {

/* Your definition for the List class goes here */
size_t List::_next_id = 0;

List::~List() {
    Logger() << "> dtor " << _id << "\n";
    clear();
    delete _dummy;
}

List::List() : _dummy(new Node()), _size(0), _id(_next_id++) {
    Logger() << "> ctor " << _id << "\n";
    _dummy->right = _dummy;
    _dummy->left  = _dummy;
}

List::List(const List& rhs) : _dummy(new Node()), _size(0), _id(_next_id) {
    Logger() << "> copy " << _id << " " << rhs._id << "\n";

    _dummy->right = _dummy;
    _dummy->left  = _dummy;

    this->_size = rhs.size();

    Node* cur     = this->_dummy;
    Node* rhs_cur = rhs._dummy->right;

    while (rhs_cur != rhs._dummy) {
        cur->right = new Node(rhs_cur->data, cur);

        cur     = cur->right;
        rhs_cur = rhs_cur->right;
    }

    cur->right   = _dummy;
    _dummy->left = cur;
}

List& List::operator=(const List& rhs) {
    Logger() << "> asgn " << _id << " " << rhs._id << "\n";
    clear();

    _dummy->right = _dummy;
    _dummy->left  = _dummy;

    this->_size = rhs.size();

    Node* cur     = this->_dummy;
    Node* rhs_cur = rhs._dummy->right;

    while (rhs_cur != rhs._dummy) {
        cur->right = new Node(rhs_cur->data, cur);

        cur     = cur->right;
        rhs_cur = rhs_cur->right;
    }

    cur->right   = _dummy;
    _dummy->left = cur;

    return *this;
}

iterator List::begin() {
    list_iterator impl(_dummy->right);
    iterator it(&impl);
    return it;
}

const_iterator List::begin() const {
    list_iterator impl(_dummy->right);
    const_iterator it(&impl);
    return it;
}

iterator List::end() {
    list_iterator impl(_dummy);
    iterator it(&impl);
    return it;
}

const_iterator List::end() const {
    list_iterator impl(_dummy);
    const_iterator it(&impl);
    return it;
}

reference List::front() { return _dummy->right->data; }

const_reference List::front() const { return _dummy->right->data; }

reference List::back() { return _dummy->left->data; }

const_reference List::back() const { return _dummy->left->data; }

size_type List::size() const { return _size; }

void List::clear() {
    Logger() << "> clear " << _id << "\n";
    Node* cur = _dummy->right;

    while (cur != _dummy) {
        Node* tmp = cur->right;
        delete cur;
        cur = tmp;
    }

    this->_size   = 0;
    _dummy->right = _dummy;
    _dummy->left  = _dummy;
}

bool List::empty() const { return _size == 0; }

void List::erase(const_iterator pos) {
    Logger() << "> erase " << _id << " " << pos - begin() << "\n";
    if (list_iterator* li_iter = dynamic_cast<list_iterator*>(pos.raw())) {
        Logger() << "> list erase " << li_iter->get_node()->data << "\n";
        Node* left_node  = li_iter->get_node()->left;
        Node* right_node = li_iter->get_node()->right;

        if (left_node != nullptr) {
            left_node->right = right_node;
        }

        if (right_node != nullptr) {
            right_node->left = left_node;
        }
        _size--;
    }
}

void List::erase(const_iterator begin, const_iterator end) {
    Node* left_node =
        dynamic_cast<list_iterator*>(begin.raw())->get_node()->left;
    Node* right_node = dynamic_cast<list_iterator*>(end.raw())->get_node();

    Logger() << "> eraserng " << _id << " " << (begin - this->begin()) << " "
             << (end - this->begin()) << "\n";

    Node* cur = left_node->right;

    while (cur != right_node) {
        Node* tmp = cur->right;
        delete cur;
        cur = tmp;
        _size--;
    }

    left_node->right = right_node;
    right_node->left = left_node;
}

void List::insert(const_iterator pos, size_type count, const_reference val) {
    Node* right_node = dynamic_cast<list_iterator*>(pos.raw())->get_node();
    Node* left_node  = right_node->left;

    Logger() << "> insert " << _id << " " << (pos - begin()) << " " << count
             << " " << val << "\n";

    Node* cur = left_node;

    while (count--) {
        cur->right = new Node(val, cur);
        cur        = cur->right;
        _size++;
    }

    cur->right       = right_node;
    right_node->left = cur;
}

void List::insert(
    const_iterator pos, const_iterator begin, const_iterator end) {

    Logger() << "> insertrng " << (pos - this->begin());

    Node* right_node = dynamic_cast<list_iterator*>(pos.raw())->get_node();
    Node* left_node  = right_node->left;

    Node* cur = left_node;

    auto it = begin;
    while (it != end) {
        cur->right = new Node(*it, cur);
        cur        = cur->right;
        Logger() << " " << cur->data;
        ++it;
        _size++;
    }

    Logger() << "\n";

    cur->right       = right_node;
    right_node->left = cur;
}

void List::pop_back() { erase(end() - 1); }

void List::pop_front() { erase(begin()); }

void List::push_back(const_reference val) { insert(end(), 1, val); }

void List::push_front(const_reference val) { insert(begin(), 1, val); }

} // namespace I2P2
