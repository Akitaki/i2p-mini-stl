#include "../header/I2P2_iterator.h"

namespace I2P2 {
vector_iterator::vector_iterator() : _ptr(nullptr) {}

iterator_impl_base& vector_iterator::operator++() {
    this->_ptr++;
    return *this;
}

iterator_impl_base& vector_iterator::operator--() {
    this->_ptr--;
    return *this;
}

iterator_impl_base& vector_iterator::operator+=(difference_type offset) {
    this->_ptr += offset;
    return *this;
}

iterator_impl_base& vector_iterator::operator-=(difference_type offset) {
    this->_ptr -= offset;
    return *this;
}

bool vector_iterator::operator==(const iterator_impl_base& rhs) const {
    return this->_ptr == dynamic_cast<const vector_iterator*>(&rhs)->_ptr;
}

bool vector_iterator::operator!=(const iterator_impl_base& rhs) const {
    return this->_ptr != dynamic_cast<const vector_iterator*>(&rhs)->_ptr;
}

bool vector_iterator::operator<(const iterator_impl_base& rhs) const {
    return this->_ptr < dynamic_cast<const vector_iterator*>(&rhs)->_ptr;
}

bool vector_iterator::operator>(const iterator_impl_base& rhs) const {
    return this->_ptr > dynamic_cast<const vector_iterator*>(&rhs)->_ptr;
}

bool vector_iterator::operator<=(const iterator_impl_base& rhs) const {
    return this->_ptr <= dynamic_cast<const vector_iterator*>(&rhs)->_ptr;
}

bool vector_iterator::operator>=(const iterator_impl_base& rhs) const {
    return this->_ptr >= dynamic_cast<const vector_iterator*>(&rhs)->_ptr;
}

difference_type
vector_iterator::operator-(const iterator_impl_base& rhs) const {
    return (this->_ptr) - (dynamic_cast<const vector_iterator*>(&rhs)->_ptr);
}

pointer vector_iterator::operator->() const { return this->_ptr; }

reference vector_iterator::operator*() const { return *(this->_ptr); }

reference vector_iterator::operator[](difference_type offset) const {
    return _ptr[offset];
}

} // namespace I2P2
