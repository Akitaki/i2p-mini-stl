#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <sstream>

namespace I2P2 {
struct Logger {
#ifndef NDEBUG
    std::stringstream ss{};
    template <typename T> inline Logger& operator<<(T&& t) {
        ss << t;
        return *this;
    }
    inline ~Logger() { std::cout << ss.str(); }
#else
    template <typename T> inline Logger& operator<<(T&& t) { return *this; }
    inline ~Logger() {}
#endif
};
} // namespace I2P2

#endif /* ifndef LOGGER_H */
