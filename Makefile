CXX := g++
CXXFLAGS := -O3 -std=c++11 -DTEST_VECTOR -DINT64 -DNDEBUG
LDFLAGS := -flto

SRC := $(wildcard ./src/*.cpp)
OBJ := $(SRC:./src/%.cpp=./build/%.o)
DEP := $(OBJ:%.o=%.d)

.PHONY: original all debug clean

original:
	@echo "> Building all *.cpp files together..."
	@echo "> Note: Use \`make all -j\` instead to build object files separately (faster)"
	g++ I2P2_main.cpp src/*.cpp -DTEST_VECTOR -DTEST_LIST -DINT64 -std=c++11 -O3

all: ./main

debug: CXXFLAGS := -DDEBUG -O0 -g -std=c++11 -DTEST_VECTOR -DINT64
debug: LDFLAGS :=
debug: all

-include $(DEP)

# Real targets

./main: ./build/main.o $(OBJ)
	@mkdir -p ./build/
	@echo "> Linking target file: $@"
	$(CXX) $^ -o $@ $(CXXFLAGS) $(LDFLAGS)

./build/%.o: ./src/%.cpp
	@mkdir -p ./build/
	@echo "> Building target file: $@"
	$(CXX) -c $< -o $@ $(CXXFLAGS) -MMD

./build/main.o: ./I2P2_main.cpp
	@mkdir -p ./build/
	@echo "> Building target file: $@"
	$(CXX) -c $< -o $@ $(CXXFLAGS) -MMD

clean:
	rm -rf ./build/ ./a.out main
