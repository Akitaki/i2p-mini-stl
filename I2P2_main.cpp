#include "header/I2P2_List.h"
#include "header/I2P2_Vector.h"
#include "header/I2P2_iterator.h"
#include "header/I2P2_test.h"

#include <cassert>
#include <cstddef>
#include <iostream>
#include <list>
#include <type_traits>
#include <random>
#include <string>

void init() {
    I2P2::Vector vec;

    {
        I2P2::Vector rhs;
        rhs.push_back(1);
        rhs.push_back(2);
        rhs.push_back(3);
        rhs.push_back(4);
        rhs.push_back(5);
        vec = rhs;
    }

    vec.dump();
}

int main() {
#ifdef TEST_LIST
    std::cout << "Checking list ..." << std::endl;
    I2P2_test::check_list();
#endif

#ifdef TEST_VECTOR
    std::cout << "Checking vector ..." << std::endl;
    I2P2_test::check_vector();
#endif

    std::cout << "Finished" << std::endl;

    return 0;
}

/* Here's how to test for both vector and list for type = double with g++:
 * g++ I2P2_main.cpp src/*.cpp -DTEST_VECTOR -DTEST_LIST -DDOUBLE -std=c++11 */
